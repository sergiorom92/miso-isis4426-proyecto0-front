# Stage 1
FROM node:10-alpine as build-step
ARG BACKEND_HOST
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN echo "export const environment = { production: false, backEndUrl: '$BACKEND_HOST' };" > ./src/environments/environment.ts
RUN echo "export const environment = { production: true, backEndUrl: '$BACKEND_HOST' };" > ./src/environments/environment.prod.ts
RUN npm run build --prod
# Stage 2
FROM nginx:1.17.1-alpine
COPY --from=build-step /app/dist/misoCloudProyecto0Front /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
