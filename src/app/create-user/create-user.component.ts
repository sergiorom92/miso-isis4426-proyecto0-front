import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { User } from '../model/user.model';
import { DialogComponent } from '../util/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private dialog: MatDialog,
    private router: Router,
  ) {}
  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', Validators.email],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      username: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      try {
        const user = new User();
        Object.assign(user, this.form.value);
        this.authService
          .createUser(user)
          .subscribe((success) =>
            success ? this.openSuccessCreateDialog() : this.openFailureDialog(),
          );
      } catch (err) {}
    } else {
    }
  }

  navigateToLogin(): Promise<boolean> {
    return this.router.navigateByUrl('/login');
  }

  openSuccessCreateDialog(): void {
    this.dialog.open(DialogComponent, {
      data: { message: 'User Created!', title: 'Success' },
    });
    this.dialog.afterAllClosed.subscribe(() => this.navigateToLogin());
  }

  openFailureDialog(): void {
    this.dialog.open(DialogComponent, {
      data: { message: 'Error creating user!', title: 'Error' },
    });
    this.dialog.afterAllClosed.subscribe(() => this.navigateToLogin());
  }
}
