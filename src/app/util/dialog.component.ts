import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-overview-example-dialog',
  template:
    '<h1 mat-dialog-title>{{data.title}}</h1>\n' +
    '<div mat-dialog-content>\n' +
    '  <p>{{data.message}}</p>\n' +
    '</div>\n' +
    '<div mat-dialog-actions>\n' +
    '  <button mat-button [mat-dialog-close]="{}" cdkFocusInitial>Ok</button>\n' +
    '</div>',
})
export class DialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogMessage) {}
}

export interface DialogMessage {
  title: string;
  message: string;
}
