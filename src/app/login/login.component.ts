import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  public loginInvalid = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      const { username, password } = this.form.value;
      this.authService
        .login(username, password)
        .pipe(first())
        .subscribe((loginValid) =>
          loginValid ? this.successLogin() : (this.loginInvalid = true),
        );
    }
  }

  private successLogin(): Promise<boolean> {
    this.loginInvalid = false;
    return this.router.navigateByUrl('events');
  }

  navigateToCreateUser(): Promise<boolean> {
    return this.router.navigateByUrl('create-user');
  }
}
