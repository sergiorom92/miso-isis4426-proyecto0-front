import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { AuthService } from './services/auth.service';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { EventDetailComponent } from './events/event-detail/event-detail.component';
import { EventEditComponent } from './events/event-edit/event-edit.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'create-user', component: CreateUserComponent },
  {
    path: 'events',
    component: EventsComponent,
    canActivate: [AuthService],
  },
  {
    path: 'events/view/:id',
    component: EventDetailComponent,
    canActivate: [AuthService],
  },
  {
    path: 'events/edit/:id',
    component: EventEditComponent,
    canActivate: [AuthService],
  },
  {
    path: 'events/create',
    component: CreateEventComponent,
    canActivate: [AuthService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
