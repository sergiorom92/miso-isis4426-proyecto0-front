import { Component, OnInit } from '@angular/core';
import { EventsService } from '../services/events.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Event } from '../model/event.model';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { DialogComponent } from '../util/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit {
  events$: BehaviorSubject<Event[]> = new BehaviorSubject<Event[]>([]);
  backEndUrl = environment.backEndUrl;
  username: string;

  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<div class="nav-btn next-slide"></div>',
    prevArrow: '<div class="nav-btn prev-slide"></div>',
    dots: true,
    infinite: true,
  };

  constructor(
    private eventsService: EventsService,
    private router: Router,
    private dialog: MatDialog,
    private auth: AuthService,
  ) {}

  ngOnInit(): void {
    this.loadEvents();
    this.username = this.auth.username;
  }

  logout(): void {
    this.auth.logout().then();
  }

  loadEvents(): void {
    this.events$ = this.eventsService.getEvents();
  }

  createEvent(): Promise<boolean> {
    return this.router.navigateByUrl('events/create');
  }

  deleteEvent(eventId: string): void {
    this.eventsService
      .deleteEvent(eventId)
      .subscribe(() => this.openSuccessDeleteDialog());
  }

  editEvent(eventId: string): Promise<boolean> {
    return this.router.navigateByUrl(`events/edit/${eventId}`);
  }

  openSuccessDeleteDialog(): Subscription {
    this.dialog.open(DialogComponent, {
      data: { message: 'Event Deleted!', title: 'Success' },
    });
    return this.dialog.afterAllClosed.subscribe(() => this.loadEvents());
  }
}
