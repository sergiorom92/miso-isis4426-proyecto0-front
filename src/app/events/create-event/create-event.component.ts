import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Event } from '../../model/event.model';
import { EventsService } from '../../services/events.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../util/dialog.component';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss'],
})
export class CreateEventComponent implements OnInit {
  form: FormGroup;
  imgURL: any;

  constructor(
    private fb: FormBuilder,
    private eventsService: EventsService,
    private router: Router,
    private location: Location,
    public dialog: MatDialog,
  ) {}
  ngOnInit(): void {
    this.form = this.fb.group({
      event_name: ['', Validators.required],
      event_category: ['', Validators.required],
      event_place: ['', Validators.required],
      event_address: ['', Validators.required],
      event_initial_date: ['', Validators.required],
      event_final_date: ['', Validators.required],
      event_type: ['', Validators.required],
      thumbnail: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      try {
        const event = new Event();
        Object.assign(event, this.form.value);
        this.eventsService
          .createEvent(event)
          .subscribe(() => this.openSuccessDialog());
      } catch (err) {}
    } else {
    }
  }

  onFileSelect(event): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imgURL = reader.result;
      };
      this.form.get('thumbnail').setValue(file);
    }
  }
  goBack(): void {
    return this.location.back();
  }

  openSuccessDialog(): void {
    this.dialog.open(DialogComponent, {
      data: { message: 'Event Created!', title: 'Success' },
    });
    this.dialog.afterAllClosed.subscribe(() =>
      this.router.navigateByUrl('events'),
    );
  }
}
