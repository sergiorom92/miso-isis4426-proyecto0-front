import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Event } from '../../model/event.model';
import { EventsService } from '../../services/events.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '@env/environment';
import { DialogComponent } from '../../util/dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss'],
})
export class EventEditComponent implements OnInit {
  form: FormGroup;
  imgURL: any;
  eventId: string;

  constructor(
    private fb: FormBuilder,
    private eventsService: EventsService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
  ) {}
  ngOnInit(): void {
    this.form = this.fb.group({
      id: ['', Validators.required],
      event_name: ['', Validators.required],
      event_category: ['', Validators.required],
      event_place: ['', Validators.required],
      event_address: ['', Validators.required],
      event_initial_date: ['', Validators.required],
      event_final_date: ['', Validators.required],
      event_type: ['', Validators.required],
      thumbnail: ['', Validators.required],
    });
    this.eventId = this.activatedRoute.snapshot.paramMap.get('id');
    this.eventsService.getSingleEvent(this.eventId).subscribe((event) => {
      if (event.id) {
        this.form.setValue(event);
        this.imgURL = `${environment.backEndUrl}/image/${event.thumbnail}`;
      }
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      try {
        const event = new Event();
        Object.assign(event, this.form.value);
        this.eventsService
          .editEvent(event, this.eventId)
          .subscribe(() => this.openSuccessDialog());
      } catch (err) {}
    } else {
    }
  }

  onFileSelect(event): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imgURL = reader.result;
      };
      this.form.get('thumbnail').setValue(file);
    }
  }
  goBack(): void {
    return this.location.back();
  }

  openSuccessDialog(): void {
    this.dialog.open(DialogComponent, {
      data: { message: 'Event Edited!', title: 'Success' },
    });
    this.dialog.afterAllClosed.subscribe(() =>
      this.router.navigateByUrl('events'),
    );
  }
}
