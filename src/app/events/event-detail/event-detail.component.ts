import { Component, OnInit } from '@angular/core';
import { EventsService } from '../../services/events.service';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Event } from '../../model/event.model';
import { environment } from '@env/environment';
import { Location } from '@angular/common';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss'],
})
export class EventDetailComponent implements OnInit {
  event$: BehaviorSubject<Event>;
  backEndUrl = environment.backEndUrl;

  constructor(
    private eventsService: EventsService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
  ) {
    this.event$ = new BehaviorSubject(new Event());
  }

  ngOnInit(): void {
    this.event$ = this.eventsService.getSingleEvent(
      this.activatedRoute.snapshot.paramMap.get('id'),
    );
  }

  goBack(): void {
    return this.location.back();
  }
}
