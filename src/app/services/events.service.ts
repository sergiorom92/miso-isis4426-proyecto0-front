import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { BehaviorSubject, Subject } from 'rxjs';
import { Event } from '../model/event.model';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  eventsAPIPath = `${environment.backEndUrl}/events`;
  constructor(private http: HttpClient) {}

  getEvents(): BehaviorSubject<Event[]> {
    const subject: BehaviorSubject<Event[]> = new BehaviorSubject([]);
    this.http
      .get(this.eventsAPIPath)
      .subscribe((data: Event[]) => subject.next(data));
    return subject;
  }

  getSingleEvent(eventId: string): BehaviorSubject<Event> {
    const subject: BehaviorSubject<Event> = new BehaviorSubject(new Event());
    this.http
      .get(`${this.eventsAPIPath}/${eventId}`)
      .subscribe((data: Event) => subject.next(data));
    return subject;
  }

  createEvent(event: Event): Subject<Event> {
    const subject: Subject<Event> = new Subject();
    const formData = new FormData();
    Object.keys(event).forEach((key) => formData.append(key, event[key]));
    this.http
      .post(this.eventsAPIPath, formData)
      .subscribe((data: Event) => subject.next(data));
    return subject;
  }

  editEvent(event: Event, eventId: string): Subject<Event> {
    const subject: Subject<Event> = new Subject();
    const formData = new FormData();
    Object.keys(event).forEach((key) => formData.append(key, event[key]));
    this.http
      .put(`${this.eventsAPIPath}/${eventId}`, formData)
      .subscribe((data: Event) => subject.next(data));
    return subject;
  }

  deleteEvent(eventId: string): Subject<Event> {
    const subject: Subject<Event> = new Subject();
    this.http
      .delete(`${this.eventsAPIPath}/${eventId}`)
      .subscribe((data: Event) => subject.next(data));
    return subject;
  }
}
