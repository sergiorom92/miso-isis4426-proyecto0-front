import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user.model';
import { catchError } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import { environment } from '@env/environment';
import { CanActivate, Router, UrlTree } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  userToken: string = null;
  username: string = null;
  constructor(private http: HttpClient, private router: Router) {}

  login(username: string, password: string): Subject<boolean> {
    const subject: Subject<boolean> = new Subject();
    this.http
      .post(`${environment.backEndUrl}/api-auth`, { username, password })
      .pipe(catchError((error) => of({ error })))
      .subscribe((data: { accessToken: string; error: string }) => {
        if (data.accessToken) {
          this.userToken = data.accessToken;
          subject.next(true);
          const tokenData = this.decodeToken(data.accessToken);
          return tokenData.username
            ? (this.username = tokenData.username)
            : null;
        }
        subject.next(false);
      });
    return subject;
  }

  logout(): Promise<boolean> {
    this.userToken = null;
    return this.router.navigateByUrl('/login');
  }

  decodeToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return {};
    }
  }

  createUser(user: User): Subject<boolean> {
    const subject: Subject<boolean> = new Subject();
    this.http
      .post(`${environment.backEndUrl}/create-user`, user)
      .pipe(catchError((error) => of({ error })))
      .subscribe((data: any) =>
        data.error ? subject.next(false) : subject.next(true),
      );
    return subject;
  }

  canActivate(): boolean | UrlTree {
    return !!this.userToken ? true : this.router.parseUrl('/login');
  }
}
